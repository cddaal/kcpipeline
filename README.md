
This project manages the knowledge component definition to be used by Project Cheetah and the Content Analytics Visualization Explorer.  It works as a pipeline:

Input:
   *  TDX IRT profiles (single file per book, as they were calculated by Quinn).  This is not clear where it lives.
   *  The TDX learning curves (one file per chapter).  These are created in Research ALE.

How it works? 

   1) Script tom.py splits books into sections and difficulty.  This script does not use additional data, it only uses the input files.
   2) Script ilya.py annotates tom.py's output with a column that indicates if the tdx is a "bad apple."   ilya.py's input is tom.py's output only.


